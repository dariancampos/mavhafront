import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable()
export class TodoService {

  private urlBase = '//localhost:8081'
  constructor(private  http: HttpClient) { }

  getAllTodo(): Observable <any> {
    return this.http.get(this.urlBase + '/todos?limit=10&offset=0');
  }

  getTodo(id: number): Observable <any> {
    return this.http.get(this.urlBase + '/todo/' + id);
  }

  saveTodo(todoData: any) {
    return this.http.post(this.urlBase + '/todo', JSON.stringify(todoData), httpOptions);
  }
  updateTodo(updateData: any, id: number) {
    return this.http.put(this.urlBase + '/todoupdate/' + id, JSON.stringify(updateData), httpOptions);
  }
}
