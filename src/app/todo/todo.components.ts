import {Component, Input, OnInit} from '@angular/core';
import {Todo} from '../model/todo.model';
import {TodoService} from '../service/todo.service';
import {formatDate} from '@angular/common';


@Component({
  selector: 'todo',
  templateUrl: './todo.components.html',
  styleUrls: ['./todo.component.css',
  ],
})

export class TodoComponent implements OnInit {
  public todos: Todo;
  public selecTodo;
  public btnEnable;
  public today = new Date();
  public  jstoday = '';
  public imgSrc;
  public imgName;
  public imgDefault;
  public search: any = '';

  constructor(private todoService: TodoService) {
    this.imgDefault = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFsAAABaCAYAAADXaio8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wMNDhoJKqEO9AAAAlJJREFUeNrt3bFLG1EAx/EXvaGKd2ogLkUHhw4KDdgoFYTaooi2ouDo3KEdBP8BKf4DHR1cdRGKCC5iNYhCRY0aoRQqBFSwtEKsl1YrDaSTXfIueYH3bvB9v2PuyPDx+CV3CIkkncaCIOP1/s1GqmAIL7DBBpvABpvABhtsAhtsAhtssAlssAlssAlssMEmsMEmsMEGm8AGm8AGm8AGG2wCG2wCG2ywCWywCWywCWywwSawwSawwQabDOSonlg33i5aJ6eVzv22PCcu3i0GHm9LzgrHjSq911FizD7sas8V0fhz5TcOwn7wqkU09YxZeWUbmZFSfxQ38YTN1l39m6fS1xs6usHWnfdYfgVHEwNg666x84V0r2tizWCb2G3nUT17Hdb3bG+kS2mv89c+2Kpl00n5h2S8q+xe56994R+nwFbtcnddPiWdfWX3+vvGAjOi4+p2W+P/dztor3/ufwJb19V9t9uxZ0PS47m9FNiV5h+lSu52tKO/6NjNxZn4s3wKdqVdzWwH7nbdeLtwar3i6dlb4auf7t2ODY5av9fasYN2++Hwa+v3Wjt20G7LJsS2vdaOHbTb0smxbK+N3K7/2PqgdJ5te20E+zK1qXSebXttBDt3UB4xl0lbt9dGsH/Nfy77FC+7+1HYmJFHrNn91dIfpOkdsMPabX8J7FB2O5dJi/zXK7DD2G1b91qICv5J5/faF3EwIX9Ment2XvTa4duXotpzlc7NvJ8SJ5Jz71sRfjUvnPjVvPuw2QQ22GAT2GAT2GCDTaHdrhcK3K2H1T9M3aX0tjPTBQAAAABJRU5ErkJggg==';
    this.jstoday = formatDate(this.today, 'dd-MM hh:mm:ss', 'en-AR');
  }


  onUploadFinish(event) {
    this.imgSrc = event.src;
    this.imgName = event.file.name;
    console.log('test Src' + this.imgSrc);
    console.log('test Name' + this.imgName);
  }

  ngOnInit(): void {
   this.getAllTodo();
   this.btnEnable = true;
  }

  getAllTodo(): void {
    this.todoService.getAllTodo().subscribe(data => {
      this.todos = data;
    });
  }

  getTodo(id): void {
    this.todoService.getTodo(id).subscribe(data => {
      this.selecTodo = data;
      this.updateTodo(this.selecTodo.id);
      console.log('Todos ID');
      console.log(this.selecTodo);
    });
  }

  @Input() todoData = { descripcion: '', estado: true, adjunto: '', inicio: '', finalizado: '' };
  addTodo() {
    if (this.imgSrc !== undefined) {
          this.todoData.adjunto = this.imgSrc;
    } else {
       this.todoData.adjunto = this.imgDefault;
    }

    this.todoData.inicio = this.jstoday;
    this.todoData.finalizado = ' - ';
    this.btnEnable = false;
    this.todoService.saveTodo(this.todoData).subscribe((result) => {
      this.btnEnable = true;
      location.reload();
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }




  @Input() updateData = {descripcion: '', estado: true, adjunto: '', inicio: '', finalizado: ''};
  updateTodo(id) {
    this.btnEnable = false;
    this.updateData.descripcion = this.selecTodo.descripcion;
    this.updateData.adjunto = this.selecTodo.adjunto;
    this.updateData.estado = !(this.selecTodo.estado);
    this.updateData.inicio = this.selecTodo.inicio;
    this.updateData.finalizado = this.jstoday;
    this.todoService.updateTodo(this.updateData, id).subscribe((result) => {
      this.btnEnable = true;
      location.reload();
      console.log(result);
    }, (err) => {
      console.log(err);
    });
  }
}
