export class Todo {
  id: bigint;
  descripcion: string;
  estado: boolean;
  adjunto: string;
  inicio: string;
  finalizado: string;
}
