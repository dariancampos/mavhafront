import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {ImageUploadModule} from 'angular2-image-upload';

// component
import {TodoComponent} from './todo/todo.components';
// Service
import {TodoService} from './service/todo.service';
import { SearchPipe } from './search.pipe';




@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    SearchPipe,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ImageUploadModule.forRoot()
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
